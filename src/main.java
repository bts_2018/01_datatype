import java.util.Date;

public class main {

	public static void main(String[] args) {
		System.out.println("============ Data Types ==================");
		
		//BYTE : This can hold whole number between -128 and 127  
		byte num_byte;
		num_byte = 113;
    	System.out.println("Byte: " + num_byte);
    	
    	//SHORT : This is greater than byte in terms of size and less than integer. Its range is -32,768 to 32767.  
    	short num_short = 32767;
    	System.out.println("Short: " + num_short);
    	
    	//INT : 2 billion to +2 billion   the most commonly used 
    	int num_int = 99999;    
    	System.out.println("Int: " + num_int);
    	
    	//LONG : 4,000 trillion to +4,000 trillion  
    	long num_long = 1000000000;   
    	System.out.println("Long: " + num_long);
    	
    	//DOUBLE :15 decimal digits  
    	double num_double = 4.2932323323; 
    	System.out.println("double: " + num_double);
    	
    	//BOOLEAN : true or false 
    	boolean b = false;
    	System.out.println("Boolean: " + b); 
    	
    	//CHAR
    	char ch = 'Z';
    	System.out.println("Char: " + ch); 
    	
    	//String
    	String str = "Hello";
    	System.out.println("String: " + str);  
    	
    	//DATE
    	Date currentDate = new Date();	
    	System.out.println("Date: " + currentDate); 
    	
    	//ARRAY
    	String[] items = {"Cheese", "Pepperoni", "Black Olives"};
        for (String item : items)
        {
            System.out.println("Item: " + item);
        }
       

    	//CLASS
        Person x = new Person();
    	x.name ="Andrea";
    	x.surname ="Gelsomino";
    	x.age = 40;
    	System.out.println("Class Person: " + x.name +  " - " + x.surname + " - " + x.age);
    	
    	
	}

}
